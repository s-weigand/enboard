Enboard is a terminal UI to easily switch between conda environments.

To install::

    pip install enboard

To use::

    enboard

- **Up** and **down** arrow keys to pick an environment
- **Enter** to enter it
- **q** to quit without entering an environment
- **/** to start a search
- **n** to create a new environment
- **Delete** to delete the one at the cursor
